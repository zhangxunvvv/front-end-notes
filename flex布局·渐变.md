  display: -webkit-box; 

 -webkit-line-clamp: 2; 

 -webkit-box-orient: vertical; 

 overflow: hidden;  

text-overflow: ellipsis;

# 

background-size：cover以短的一方进行拉伸，一直到充满盒子，contain 长的一方拉伸
upload  阿普路德
移动端布局；读音：弗莱克斯

web前端移动端基础

# 视口 理想视口 meta视口标签



# flex布局

 弹性布局 任何元素都可以指定flex  一行显示，不用清除浮动，自适应float  clear   vertical-align 将会失效

display：flex  给父元素添加  

## flex-directron 设置主轴方向

  row    X轴默认从左到右   

roe-reverse 从右到左(反转)   

设置Y轴 : column 从上到下  

column-reversr从下到上反转

## justify-content 设置主轴上子元素排方式  

flex-start 默认值，从左到右  

flex-end   从右到左对齐  

center 在主轴居中对齐  

space-around  平分剩余空间（平分外边距）  

space-between 先两边贴边再平分剩余空间

## flex-wrap：

nowrap默认不换行 ， wrap换行  flex可以写成百分比

## align-items 给父元素

####   单行设置子元素侧轴对齐方式   

flex-start 侧轴从上到下   

flex-end 侧轴从下到上  

 center 挤在一起侧轴居中   

stretch 侧轴拉伸 子盒子不能设置高度

## align-content 换行/​多行的情况下设置子项对齐方式    

space-around 子项在侧轴平分  多行居中 

   space-between 子项在侧轴先分布在两头，再平分

### 给子元素的 flex：

2 没有默认值  分配剩余空间

#### align-self:flex-end 

控制侧轴单个盒子对齐order 决定子元素显示顺序 默认为0  数值越小越靠前



## 渐变色  to left改变渐变方向

background: linear-gradient(to left , red,green)

#### 两个必须装的插件

easy less 自动生成lass文件

px2rem 0.03 自动转化为rem单位版本视频中低版本  新版有b-g



# rem

rem相对于html元素的字体大小决定页面全部用rem的大小

html的font-size 决定页面中rem单位的大小

# 媒体查询

all 所有设备     print   打印机      screen  电脑屏幕

and 多个媒体相当于“且”  not 排除某个  only 指定特定的某个

widtgh 可见宽度  min-width 最小可见宽度 max-width 最大可见宽度

@media screen and (max-widthh : 空格 1200px) {

div {

background-color:green

}

}

媒体查询引入资源  了解即可

![image-20210322154451480](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210322154451480.png)

# less的安装以及简单使用  ：

#### 插件的安装使用：

官网http://lesscss.cn 

新建一个后缀.less的文件 

![image-20210322161754422](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210322161754422.png)



@color:pink  调用这个@color变量名字

命名会区分大小写，

less不能直接引用，因为浏览器不识别

想用先生成css文件 然后导入到页面

安装插件easy less  1.7.2

less写代码，自动生成写入一个css文件

导入这个css文件。

#####   less嵌套

.box{.a{.b{}}}可以直接在lass里面嵌套。

伪元素 用&::before 直接在父盒子里面写

##### 导入 @import 

在生成的lesss里面写@import “common.less”把另一个css导入过来

# 响应式！bootstrop框架

#### 起步下载生产环境 版本3和4不通用，不能互相引用

.cos-xs-<768    sm>768   md>992  lg>1200      

快捷键wen+r输入cmd

container 版心永远居中   container-fluid通栏

row包含col-lg-3

嵌套用row包含row包含row

列嵌套高度不能继承

![image-20210325103416042](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210325103416042.png)

![image-20210325103450085](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210325103450085.png)



### 列偏移6个 .col-lg-offset-6

噢复塞特

### 列排序。col-lg-6

col-lg-pull-6往左偏移6

col-lg-push-6往右偏移6



# js基础第一天大小写敏感

### javascript  > typescript

if else for while

script双标签

#### alert ('你好')

alert (’你好‘)   弹出动画框  额劳特

数据存储单位 

位:1bit可以保存一个0或者1  1b=8b

ECMAscript dom bom

外联式<script src="my.js"></script>

行内式  input type="button" value="唐伯虎" onclick=“alert”(秋香姐'')

齿轮>键盘快捷方式 设置快捷键

crtl+b 快速关闭资源管理器 

alt+上下方向键 快速上下移动代码

shift+alt+上下方向键 快速复制

### 输入输出

alert (msg) 弹出框

console.log(msg) 浏览器打印输出信息 程序员测试使用

prompt(info) 弹出输入框 用户输入 

### 变量 var

var age；声明一个名称为age的变量

age=10；给age赋值为10

= 是赋值的意思，不是相等 尽量不要用name44

var age =18；声明变量同时赋值

var age =18;

age=81;

结果为81，因为18被覆盖掉了

只声明不赋值会导致undefined 未定义的

一次声明多个变量用逗号分开

先声明后使用，var name prompt('请输入')

const常量不能被修改 否则报错 必须有初始值

下面有波浪线就是写法有误

temp声明一个变量为空

### 数据类型



js的变量数据类型是在程序运行当中根据等号右边的值判断的

var a = 10；  数字型或数值型 number  默认0

var a ='pink'   字符串型  string  

boolean  布尔值 代表真假 flase true默认flase 等价0和1

undefined 声明了但是没有给值

null  空值    属于object 数据类型

typeof 检测数值类型 log(typeof A)



#### log输出的文字是黑色代表字符型

蓝色是数值型



### 数字型特殊情况

八进制 0开头 逢7进1 010就是8 

十六进制 0x十六进制

number.max_value数值最大值 min_value最小值

*2就是无穷大    负的 * 2就是无穷小  nan非数字

console.log(isnan(pink老师))判断是否非数字false假 true真 

嵌套关系外双内单或者外单内双

#### 转义符

都是\开头，都写在引号里面

\n换行   \ \斜杠   \ '单引号   \ "双引号   \t缩进   \b空格    \r\n也是换行  

单引号不支持换行 反引号支持换行

log(a.length)获取整个字符串长度

数值相加 字符相连

log ('pink' + age + '岁') 引引加加

## 数值转换

##### 转换为字符串

变量·tostring()      string(变量)     和字符串相加

##### 转成数值型

log(parseInt(变量)函数)将字符串类型转化为整数数值型自动去掉后面的不是数字的文字，前面是文字会（nan）

parseFloat(string) 将string转化为浮点数数值型

Nunber() 强制转换函数 将string类型转换为数值型

js隐式转换(- * /) 利用算数运算隐式转换为数值型 

示例    log(parseInt(age));

#### boolean（）布尔值转换

没有值或者否定的是false  有值的就是true

&&且的意思   | |或的意思     !  非

&&两侧有一个fales就是假

### 三元表达式

条件表达式 ？ 表达式1真 ： 表达式2假

var num =10

var ccc=num>10 ? 是 : 否

alert(ccc)

age +=5相当于age=age+5

age-=5相当于age=age-5

age*=10相当于age * 10





### switch多分支语句，可以多选一

switch  开关的意思

switch(表达式) {

   case value1:

​            执行语句1;

​            break;

​    case value2:

​             执行语句2;

​             break;

​              ...

​             default:执行最后语句

}













# js-循环 

## for循环 非常重要

for (var i = i; i <= 1000; i++) {

console.log('媳妇我错了')}

重复执行某些代码，跟(计数)次数有关系

#### for （ 初始化变量； 条件表达式；操作表达式 ）

 初始化 就是用var 声明的一个普通变量 通常作为计数器

条件表达式 决定每次循环是否继续或终止

操作表达式 每次循环最后执行的代码 经常用于计数器变量的更新（递增或递减） 

## while循环

while (条件表达式) {循环体}

var a=1

while (a<=100) {

a++、                                  

}

### 循环退出循环关键字

continue 立即退出本次循环，开始下一次循环 

break 退出整个循环

## 数组

数组是从0开始计算的，

var a = [0,1,2,3,4]   必须有逗号

筛选第三个       log(a[2])

如果全部访问一遍就用for循环，

注意i必须从0开始，小于最大值log(a[i])

#### 数组的数量，用 数组名.length

 查询数组数量（长度)   a.length

a.length = 5   增加数组数量

a[3] = ‘pink’    追加一个新的数组

新的覆盖老   老的会被替换

新字符串覆盖数组名会直接覆盖数组！

## 冒泡排序







## 函数

声明函数 function  函数名(形参1){函数体}

调用函数     函数名（实参1）

函数不调用不生效。

封装就是以函数的方式封装起来

形参，实参

#### 函数的返回值

function 函数名(){

return需要返回的结果

}

只要函数遇到return 就把后面的结果返回给调用者。

return 终止函数 只能返回一个值

函数没有teturn 返回的是undefined