## jQ的优点

![image-20210416205302181](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416205302181.png)

## jQ的基本使用

https://code.jquery.com//

引入jQuery

![image-20210417093151057](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417093151057.png)

页面加载完加载js文件  传统模式

![image-20210416205916834](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416205916834.png)

#### 页面加载完

第二种方式,更简单

$(function(){

$('div').hide() 

})



$('div').hide() 隐藏元素

![image-20210416210123120](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416210123120.png)

![image-20210416210335802](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416210335802.png)

#### jQ对象和DOM对象的转化

![image-20210416211300391](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416211300391.png)

![image-20210417103705912](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417103705912.png)

jQ对象转化为DOM对象

![image-20210416211413651](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416211413651.png)

## jQuery选择器

![image-20210416211629836](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416211629836.png)

![image-20210416212008948](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416212008948.png)

#### 知识铺垫

$('div').css('background','pink')   获取所有div

​    给所有div设置背景为pink  

​    隐式迭代     内部隐式默认遍历循环

#### 筛选选择器

$('ul li:first').css('color','red')

![image-20210416212603838](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416212603838.png)

#### jq筛选方法

![image-20210417100716189](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417100716189.png)

.pernt 亲爸爸

.parents('.yeye')   选出所有所有父元素

![image-20210417095500342](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417095500342.png)

children  亲儿子

![image-20210417095447602](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417095447602.png)

.find ('p') 类似于后代选择器,所有子元素

​    

![image-20210417113718727](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417113718727.png)

#### 兄弟

.siblings('li')  其他兄弟元素  除了自身元素

选取某一个元素 eq() 写外面

![image-20210417141047309](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417141047309.png)

#### 新浪下拉菜单

$(this) 没有引号 

.show()   显示

.hide()  隐藏  

![image-20210417100127636](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417100127636.png)

![image-20210417100558131](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417100558131.png)

#### 排他思想

![image-20210417101012483](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417101012483.png)

#### jQ插件 

Jquery Code Snippets插件

![image-20210417114848744](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417114848744.png)

#### 淘宝精品服饰

$(this).index()获得元素索引号

![image-20210417144007696](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417144007696.png)

操作css的注意事项

![image-20210417152033692](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417152033692.png)

#### 操作css方法

$('div').css({

width: 400,

hright:400,

值不是数字必须加引号

backgroundColor: 'red'

复合属性必须驼峰命名法

})

#### addClass

追加类名

![image-20210417152705043](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417152705043.png)

删除类名

![image-20210417152752769](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417152752769.png)

切换类名  无添加   有删除

![image-20210417152826960](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417152826960.png)

![image-20210417162405558](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417162405558.png)

#### 判断类名存在

.hasClass('liactive') 判断类名是否存在,true false

## 动画效果

jQuery

![image-20210417162530289](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417162530289.png)

#### show显示

![image-20210417162655667](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417162655667.png)

.hide(时间,回调函数)

.show(时间,回调函数)

.toggle(1000)开关

一般都是不跟参数,太丑

![image-20210417163131915](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417163131915.png)

#### 滑动效果

slideDown ()下拉出来

slideUp()  上拉

slideToggle(500)切换  

![image-20210418140256156](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210418140256156.png)

## 事件切换

.hover(经过函数,离开函数)

![image-20210417163912014](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210417163912014.png)

.hover(funct...)鼠标经过离开都会触发

#### 停止动画stop()

stop()必须写到动画前面

![image-20210418140611019](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210418140611019.png)

![image-20210418140546781](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210418140546781.png)

#### 淡入淡出

fadeIn(1000)淡入   逐渐显示进来  

fadeOut(1000)淡出    逐渐隐藏出去

fadeToggle(1000,0.4) 淡入淡出切换

fadeTo(1000,0.5)  修改透明度.1秒变换时间





![image-20210418211821135](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210418211821135.png)

![image-20210418141725454](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210418141725454.png)

![image-20210418141955283](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210418141955283.png)

![image-20210418142835410](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210418142835410.png)

#### 自定义动画

![image-20210418211905925](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210418211905925.png)

![image-20210418212047195](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210418212047195.png)

#### 王者手风琴

![image-20210418212841138](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210418212841138.png)

## 属性操作

#### 固有属性

prop('属性')获取属性值

prop('属性','属性值') 设置属性值

.prop('checked')获取属性状态.真假

#### 自定义属性

.attr('index')获取属性值

.attr('属性','属性值') 设置属性值

#### 数据缓存

.data('uname','andyy')存储到内存

.data('index') 获取h5新增不用写data-

​          返回的是数值型

#### 复选框选中个数

change事件    

:checked 选择器  获取被选中的复选框

![image-20210418222251557](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210418222251557.png)

## 设置元素内容

#### .html

.html()获取元素内容

.html('9999') 设置元素内容

#### .text()  

.text()获取元素内容

.text('9999') 设置元素内容

#### 设置表单值

.val()获取元素内容

.val('9999')设置元素内容

![image-20210419112044644](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419112044644.png)

substr(1)截取字符串

parents()祖先级元素

toFixed(2)保留两位小数

parseInt()转换数值型

.typeof 检测类型

## jQ元素操作

#### 遍历元素 

​    index   索引号名字  

​    domEle   dom元素对象 

.eath(function(index,domEle){xxx;})

![image-20210419134534008](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419134534008.png)

![image-20210419151950895](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419151950895.png)

![image-20210419152042808](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419152042808.png)

![image-20210419152312487](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419152312487.png)

$.each($('div'),function(index,value){})



#### 创建元素

![image-20210419160957341](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419160957341.png)

#### 追加元素

![image-20210419161030639](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419161030639.png)

#### 添加到前面

.prepend(li)

![image-20210419161102834](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419161102834.png)

#### 添加兄弟后面

.after(div)

![image-20210419161242558](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419161242558.png)

#### 添加兄弟前面

.befory(div)

![image-20210419161250836](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419161250836.png)

#### 删除元素

.remove()

![image-20210419161351361](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419161351361.png)

#### 删除子节点

.empty()

![image-20210419161420387](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419161420387.png)

删除子节点

.html("")

![image-20210419161429780](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419161429780.png)

#### 元素之前兄弟

prevAll()

#### 元素之后兄弟

nextAll() 

splice()





## jQ尺寸



![image-20210419164332322](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210419164332322.png)

:selected 被选中的元素

## 位置偏移

距离文档的位置,跟父盒子没有关系

.offset().left

.offset().top

设置

.offset(){

top: 200

left: 200

}

![image-20210420091952204](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420091952204.png)

position()  父级有定位的坐标,父级没有按文档

只能获取不能设置

#### 卷去头部

![image-20210420092553455](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420092553455.png)

![image-20210420092709497](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420092709497.png)

![image-20210420093006177](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420093006177.png)

.scrollTOp(100)赋值

#### 返回动画

![image-20210420093530610](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420093530610.png)

#### 电梯导航

![image-20210420102335554](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420102335554.png)



##  事件委托

#### 事件注册

jq+on快捷键



.on({

mouseenter: fun(){



}

})

鼠标经过离开触发

![image-20210420111511249](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420111511249.png)



![image-20210420111317867](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420111317867.png)

![image-20210420111140713](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420111140713.png)

![image-20210420110924290](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420110924290.png)

#### 事件委派



委托事件给ul  触发li

![image-20210420111929850](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420111929850.png)



给未来动态元素创建绑定事件![image-20210420112302956](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420112302956.png)

![image-20210420115425858](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420115425858.png)

JSON.stringify 转化字符串   数据发送发服务器1

JSon.parse 转化成数组   服务器返回数据

typeof 检测

## 本地存储

localStorage 本地存储 永不丢失

 localStorage.setItem('t',tocccc)

## 拷贝对象

$.extend(浅拷贝,拷贝对象,待拷贝对象,)

true为深拷贝



![image-20210421092759530](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210421092759530.png)

## jQ多库共存

$改为 jQuery

自定义$符号

var suibian = $.noConflict()



![image-20210422104028442](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210422104028442.png)

#### 全屏滚动



![image-20210421093415048](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210421093415048.png)

http://ww Head w.dowebok.com/demo/2014/77/

## 图片懒加载

![image-20210421114224589](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210421114224589.png)

ctrl+h替换

先有图片再导入到页面最下面

jq22







组件

layui插件库