pageX 鼠标相对于文档的位置

mousedown 鼠标按下

mousemove 鼠标移动触发

mouseup 松开鼠标触发

offsetParent 父元素有定位返回父元素,没有返body

offsetTop 返回元素有定位的父元素上方的偏移

offsetLeft 左边的偏移

offsetWidth 返回自身宽度,包括padding border

offsetHeight 自身高度 包括padding...... 

continue 跳过本次循环,执行下一次循环

resize   窗口大小发生改变事件

DOMContentLoaded 主要dom元素加载完毕

pageshow  重新加载页面触发的页面pre

removeEventListener('事件',事件函数名)  删除某个事件

1.offset系列 经常用于获得元素位置    offsetLeft  offsetTop

2.client经常用于获取元素大小  clientWidth clientHeight

3.scroll 经常用于获取滚动距离 scrollTop  scrollLeft  

4.注意页面滚动的距离通过 window.pageYOffset  获得

![image-20210413141114037](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210413141114037.png)