#  Git 笔记

# 1.仓库初始化

```shell
git init
```

# 2.查看状态

```shell
git status 
git status -s #精简展示6
```

# 3.添加到暂存区

```shell
git add 文件名字
git add .  # 添加多个到暂存区
```

# 4.提交到仓库

```shell
git commit -m '文件描述'
git commit -a -m '文件描述' #跳过暂存区,必须是已经追踪过的文件
```

# 5.取消暂存的文件

```shell
git reset HEAD 要移出的文件名称
```

#  6.移除文件

```shell
# 从 Git仓库和工作区中同时移除 index.js 文件
git rm -f index.js
# 只从 Git 仓库中移除 index.css，但保留工作区中的 index.css 文件
git rm --cached index.css
```

# 7.查看提交历史

```shell
# 按时间先后顺序列出所有的提交历史，最近的提交在最上面
git log

# 只展示最新的两条提交历史，数字可以按需进行填写
git log -2

# 在一行上展示最近两条提交历史的信息
git log -2 --pretty=oneline

# 在一行上展示最近两条提交历史信息，并自定义输出的格式
# &h 提交的简写哈希值  %an 作者名字  %ar 作者修订日志  %s 提交说明
git log -2 --pretty=format:"%h | %an | %ar | %s"
```

# 8.回退到指定的版本

```shell
# 在一行上展示所有的提交历史
git log --pretty=oneline

# 使用 git reset --hard 命令，根据指定的提交 ID 回退到指定版本
git reset --hard <CommitID>

# 在旧版本中使用 git reflog --pretty=oneline 命令，查看命令操作的历史
git reflog --pretty=onelone

# 再次根据最新的提交 ID，跳转到最新的版本
git reset --hard <CommitID>
```

# 9.,设置远程仓库

```shell
git remote add origin 仓库url链接
git push -u origin master  //-u 记住 仓库链接和分支
```

# 10.查看分支列表

```shell
git branch 
```

# 11.创建分支

```shell
git branch 分支名字
```

# 12.切换分支

```shell
git checkout 分支名字
```

# 13.快速创建和切换分支

```shell
git checkout -b 分支名字
```

# 14.合并分支

```shell
git merge 分支名字
```

# 15.删除分支

```shell
git branch -d 分支名字 #当前分支必须是合并过的分支
git branch -D 分支名字 #强制删除
```

# 16.将本地分支推送到远程仓库

```shell
git push -u origin login：logontest
git push -u origin 本地分支名字：远程分支的名字（可以不写）
```

# 17.查看远程仓库分支的名字列表

```shell
git remote show 仓库的名字（origin）
```

# 18.将远程仓库的分支下载到本地

```shell
# 示例
git checkout 分支名字

# 从远程仓库中，把对应的远程分支下载到本地仓库，并把下载的本地分支进行重命名
git checkout -b 本地分支名称 远程仓库名称/远程分支名称

# 示例
git checkout -b payment origin/pay
```

# 19.更新代码 

```shell
git pull
```

# 20.删除远程分支

```shell
# 删除远程仓库中，制定名称的远程分支
git push 远程仓库名称 --delete 远程分支名称

# 示例
git push origin --delete pay
```

