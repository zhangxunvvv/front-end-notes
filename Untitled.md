## url地址

统一资源定位符

url组成的三部分

1客户端与服务器的通信协议

2存有该资源的服务器名称

3资源在服务器上的具体位置

![image-20210502203245785](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502203245785.png)

 **请求-处理-响应**

#### xhr对象

XMLHttpRequest 请求服务器数据资源

![image-20210502204356002](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502204356002.png)



#### get请求 获取

获取服务器资源

#### post 请求  发送

向服务器提交资源