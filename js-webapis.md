# 事件的处理

## 鼠标事件

 element.style.color='red'

var btn = document.getElementById('btn');//获取html里面的id标签

   btn.onclick = function () {

​    alert('点秋香');

   };

click 鼠标点击

mouseover 经过

​      mouseenter 鼠标经过

mouseout 离开

​     mouseleave 离开

focus 获得焦点

blur 失去焦点

mousemove 鼠标移动

mouseup 鼠标弹起

mousedown 鼠标按下



#### 鼠标x,y坐标

![image-20210410151204260](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210410151204260.png)

window是最大的对象

## Math 对象方法 

| 方法                                                         | 描述                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| [abs(x)](https://www.w3school.com.cn/jsref/jsref_abs.asp)    | 返回数的绝对值。                                             |
| [acos(x)](https://www.w3school.com.cn/jsref/jsref_acos.asp)  | 返回数的反余弦值。                                           |
| [asin(x)](https://www.w3school.com.cn/jsref/jsref_asin.asp)  | 返回数的反正弦值。                                           |
| [atan(x)](https://www.w3school.com.cn/jsref/jsref_atan.asp)  | 以介于 -PI/2 与 PI/2 弧度之间的数值来返回 x 的反正切值。     |
| [atan2(y,x)](https://www.w3school.com.cn/jsref/jsref_atan2.asp) | 返回从 x 轴到点 (x,y) 的角度（介于 -PI/2 与 PI/2 弧度之间）。 |
| [ceil(x)](https://www.w3school.com.cn/jsref/jsref_ceil.asp)  | 对数进行上舍入。                                             |
| [cos(x)](https://www.w3school.com.cn/jsref/jsref_cos.asp)    | 返回数的余弦。                                               |
| [exp(x)](https://www.w3school.com.cn/jsref/jsref_exp.asp)    | 返回 e 的指数。                                              |
| [floor(x)](https://www.w3school.com.cn/jsref/jsref_floor.asp) | 对数进行下舍入。                                             |
| [log(x)](https://www.w3school.com.cn/jsref/jsref_log.asp)    | 返回数的自然对数（底为e）。                                  |
| [max(x,y)](https://www.w3school.com.cn/jsref/jsref_max.asp)  | 返回 x 和 y 中的最高值。                                     |
| [min(x,y)](https://www.w3school.com.cn/jsref/jsref_min.asp)  | 返回 x 和 y 中的最低值。                                     |
| [pow(x,y)](https://www.w3school.com.cn/jsref/jsref_pow.asp)  | 返回 x 的 y 次幂。                                           |
| [random()](https://www.w3school.com.cn/jsref/jsref_random.asp) | 返回 0 ~ 1 之间的随机数。                                    |
| [round(x)](https://www.w3school.com.cn/jsref/jsref_round.asp) | 把数四舍五入为最接近的整数。                                 |
| [sin(x)](https://www.w3school.com.cn/jsref/jsref_sin.asp)    | 返回数的正弦。                                               |
| [sqrt(x)](https://www.w3school.com.cn/jsref/jsref_sqrt.asp)  | 返回数的平方根。                                             |
| [tan(x)](https://www.w3school.com.cn/jsref/jsref_tan.asp)    | 返回角的正切。                                               |
| [toSource()](https://www.w3school.com.cn/jsref/jsref_tosource_math.asp) | 返回该对象的源代码。                                         |
| [valueOf()](https://www.w3school.com.cn/jsref/jsref_valueof_math.asp) | 返回 Math 对象的原始值。                                     |

## 元素节点

#### parentNode  获得元素最近的亲爹

#### children[0]  获得亲儿子    非标准但是常用

.nextElementsibling    IE9以上支持 获得下一个元素节点

.previousElementsibling  IE9以上 获得上一个元素节点

## 创建删除节点

#### 创建一个节点

var li = document.createElement('li'创建一个

#### 后面添加子

父级node.appendChild(子级) 后面追加子元素

#### 前面添加子

父级node.insertBefore(创建的,指定元素) 

node.insertBefore(元素名,ul.children)此元素前面

#### 删除子后返回子

#### node.removeChild(删除节点)

#### 克隆节点

node.cloneNode()浅拷贝

node.cloneNode(true)深拷贝 

注意克隆后要添加.

#### 移除事件

removeEventListener('click',fun) 移除事件

需要给移除的事件单独设置函数名

## 自定义属性

 .getAttribute('index') 获得自定义内置属性

.setAttribute('属性名','值') 设置自定义属性

用var会导致window关键字冲突

## 阻止:冒泡,默认

e.preventDefault()阻止默认行为

e.stopPropagation() 阻止冒泡

e.target 返回触发事件

e.type 返回事件类型,如 click

## 键盘对象事件

onkeyup 松开键盘

onkeydown 按下 不区分大小写 优先级1

onkeypress 按下 区分大小写

​          不能识别功能键,如Ctrl 箭头 

e.keyCode 返回Ascll值判断按下的哪个键

# Bom

![image-20210410141055586](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210410141055586.png)



页面加载事件

![image-20210410142940118](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210410142940118.png)

## 调整窗口大小事件

#### resize窗口

window.innerWidth  浏览器窗口宽度

window.innerHeight  浏览器窗口高度

窗口大小改变事件 window.onressize

![image-20210410144231692](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210410144231692.png)

innerWidth  浏览器可视区域宽度

innerHeight 浏览器可视区域高度

![image-20210410144502748](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210410144502748.png)

## 定时器

#### 延时器 定时器

setTimeout()

两秒后调用函数  单位毫秒

window可以省略

window.setTimeout(function(){

alert('爆炸了')

}   2000)

另一种写法,

![image-20210410151555456](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210410151555456.png)

不提倡的写法:

![image-20210410151654445](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210410151654445.png)

js运行机制-有回调函数=>异步函数

异步函数等同步函数运行完毕才会运行

![image-20210410152716702](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210410152716702.png)



#### 	清除定时器

名字要自己定义

clearTimeout(定时器名字)

clearInterval(反复调用的定时器名字)

#### 反复调用定时

setInterval  定时器隔一段时间就调用一次

window.setInterval (function(){

alert('爆炸了')

}   2000)

setInterval   不断变化

倒计时先调用一次时间函数防止加载和有空白

![image-20210410162757790](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210410162757790.png)

![image-20210410163351243](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210410163351243.png)

定时器,延时器

![image-20210412091740043](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210412091740043.png)

## 同步和异步

![image-20210412093829815](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210412093829815.png)

event loop事件循环  同步异步 js执行

![image-20210412094957644](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210412094957644.png)

three.js游戏开发框架

## 链接地址对象

#### location对象

![image-20210412101351910](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210412101351910.png)

##### 属性

![](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210412101743483.png)

##### 方法

![image-20210412113131866](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210412113131866.png)

## 历史记录交互对象

#### 网页前进后退

history对象

history.forward() 前进

.back () 后退

go(1)   1前进一步负数是后退





![image-20210412114502337](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210412114502337.png)

## 模态框

鼠标按住后移动事件   ,

​     注意鼠标事件是相对于document

![image-20210416182922649](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416182922649.png)

## offset偏移量

![image-20210412142309618](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210412142309618.png)

![image-20210412172911811](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210412172911811.png)

## 放大镜效果



![image-20210412172944731](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210412172944731.png)

## 常用单词汇总 

pageX 鼠标相对于文档的位置

mousedown 鼠标按下

mousemove 鼠标移动触发

mouseup 松开鼠标触发

offsetParent 父元素有定位返回父元素,没有返body

offsetTop 返回元素有定位的父元素上方的偏移

offsetLeft 左边的偏移

offsetWidth 返回自身宽度,包括padding border

offsetHeight 自身高度 包括padding...... 

continue 跳过本次循环,执行下一次循环

resize   窗口大小发生改变事件

DOMContentLoaded 主要dom元素加载完毕

pageshow  重新加载页面触发的页面pre

removeEventListener('事件',事件函数名)

## client内容大小

client经常用于获取元素大小  clientWidth clientHeight

![image-20210412205802850](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210412205802850.png)

![image-20210412212901278](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210412212901278.png)

xiaopiu模板网站

## 立即执行函数

(function(形参){})(实参)立即执行函数,自调用

独立创建了一个作用域,不会命名冲突.

尽量在前面加个分号.预防别人没写分号

## scroll滚动距离

scroll 经常用于获取滚动距离 

​          scrollTop  scrollLeft 



#### window.pageYOffset  

注意页面滚动的距离通过 window.pageYOffset  获得

![image-20210413103735659](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210413103735659.png)

![image-20210413104306133](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210413104306133.png) 

1.offset系列 经常用于获得元素位置    offsetLeft  offsetTop

2.client经常用于获取元素大小  clientWidth clientHeight

3.scroll 经常用于获取滚动距离 scrollTop  scrollLeft  

4.注意页面滚动的距离通过 window.pageYOffset  获得

![image-20210413140928166](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210413140928166.png)

## 动画函数

setlnterval 定时器 setlnterval (fun,3000)

clearInterval(函数名) 清除定时器需要单独设置函数名

![image-20210413144528590](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210413144528590.png)

### 缓动动画

![image-20210413152015965](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210413152015965.png)

 步长改成整数

Math.ceil 往上取整

Math.floor 往下取整



![image-20210413164240437](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210413164240437.png)

mouseenter  鼠标经过   不冒泡

## 轮播图

children.length 子元素长度

document.createElement('li')创建

ol.appendChild('li')添加

ol.children[0] ol里面第一个li

className = '类名' 添加类名

排他思想排除其他人,复活我自己

ctrl + f 查找

ctrl + g跳转行 

函数名.事件 手动调用点击事件

​                         

## 节流阀

![image-20210414114147559](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210414114147559.png)

## Data  时间对象

***\*内置对象date创建\****：

var date =new Date(); Date日期对象 是个函数 因此需要New关键字创建

function daojishi(time){ var d1=+new Date();var d2=+new Date(time);var miao=(d2-d1)/1000;

d= parseInt(miao/60/60/24);h= parseInt(miao/60/60%24);

m=parseInt(miao/60%60);s= parseInt( miao%60);return d+"天"+h+"时"+m+"分"+s+"秒"}

var arr=[]; var arr1=new Array();//空数组 

var arr2=new Array(2);长度为 2 的数组有2个空的元素

var arr3=new Array(2,3); 元素为 2和 3

 var a1=[1,2,3,4];instanceof 运算符; a1 instanceof Array

isArray() 方法 ;Array.isArray(a1); //h5新增的 ie9 以上支持 常用

## 淘宝返回顶部

#### window.scroll (x,y)

​        窗口滚动到指定位置

最上方就是Window.scroll (0,0)

直接写数字,不带单位

window.pageYOffset

# 移动端js

mui框架             

interval 定时播放

不考虑兼容性.

touch 触摸     ta chi



## 触屏事件TouchEvent

**touchstart**    **手指触摸触发事件**

**touchmove 滑动触发事件**

**touchend 手指移开触发事件**

 **touchcancel    触摸被取消时触发，**

​       例如当用户将他们的手指移动到屏幕之外时。



e.touches 正在触摸屏幕的所有手指列表

e.**targetTouches**正在触摸dom元素手指列表

​         e.targetTouches[0] 第一个手指元素的坐标

​           pageX     pageY   手指的坐标

![image-20210415153244931](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210415153244931.png)

e.changedTouches手指状态发生改变,

​                手指状态从有到无  从无到有

e.preventDefault();阻止默认的屏幕滚动

<img src="C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210414144757912.png" alt="image-20210414144757912" style="zoom:150%;" />

![image-20210414145417644](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210414145417644.png)

![image-20210414145747965](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210414145747965.png)

ul.style.transform='translateX('+移动距离+'px)'

setInterval(fun(){执行体})设定时器两秒移动一次 

.transition = 'all .3s'  过渡动画

.transition = 'none'  去掉过渡动画

#### 监听过渡事件检测

#### 过渡执行完毕执行

ul.addEvent...('transitionend',fun(){

log(1) 过渡执行完毕执行打印

})

transitionend  过渡执行完毕事件

## classList新增属性

classList   返回元素的类名.ie10以上支持

​                           class:a b"

**div.classList[0]**  元素的第一个类名    a

**div.classList.add('three')**    three类名

​                追加一个类名 不会覆盖

​                注意three类名前面不要加点

**div.classList.remove('one')**删除某一个类名

 **div.classList.toggle('bg')**    bg是类名

​            切换类名  替换类名  自动检测 

​           有类名删除,没有就添加

![image-20210416091451082](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416091451082.png)

#### 返回顶部

![image-20210416102315307](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416102315307.png)

![image-20210416102341230](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416102341230.png)

## 移动端click延时问题

视口标签添加禁用缩放

user-scalable=no

![image-20210416103305862](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416103305862.png)

![image-20210415201351516](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210415201351516.png)

#### touch事件封装          

#### 

![image-20210415201127306](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210415201127306.png)

#### fastclick延时插件 

​    就是一个js文件  移动端延迟 移动端延时

![image-20210415201527732](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210415201527732.png)

​                  lib库里面   fastclick.js文件



## Swiper轮播图插件

![image-20210415201914937](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210415201914937.png)

获取swiper   下载 最新版

引入css   js  文件   html结构

官网  获取swiper     zepto移动端提供

layui 前端框架

![image-20210415203303396](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210415203303396.png)

![image-20210415203315303](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210415203315303.png)



#### 视频插件

zy.media.js

///

![image-20210416140719565](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416140719565.png)





# 移动端框架

<img src="C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210415204253888.png" alt="image-20210415204253888" style="zoom:150%;" />

Bootstrap

Zepto插件

​          轻敲事件 本质就是触摸事件

## 数据存储

#### 本地存储

数据储存在浏览器中 

设置读取方便,甚至刷新不丢失数据

容量大 sessionStorage  5M

​             localStorage       20M

只能存储字符串 

#### 临时数据

**sessionStorage 数据的存储以及获取**

Window.sessionStorage 

1.生命周期为关闭浏览器窗口

2 同一窗口(页面)下数据可以共享

3以键值对形式存储

**window对象的** 

val = ipt.value

添加数据



**sessionStorage.setItem(key,value)**

key 名字  value  值  ('uname',val)

获取数据   

sessionStorage. getItem(kuname)

删除数据

sessionStorage. removeItem(kuname)

!!!!!!!!   删除所有数据   慎用!!!!!

sessionStorage.clear()括号不写东西



#### 永久有效

**localStorage 数据的存储以及获取**

W'indow.localStorage

使用方法和上面那个一模一样

生命周期永久有效

多窗口共享(谷歌)  浏览器不同效果可能不一样

键值对      字符串

![image-20210415211848475](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210415211848475.png)

#### 案例 记住名字

** \**

#### change 事件

当复选框发生改变会触发的事件 用来判断复选框是否勾选     

![image-20210415212851814](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210415212851814.png)



将数组转成字符穿串具有数组结构

![image-20210416154513075](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416154513075.png)

![image-20210416155413017](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210416155413017.png)

.push()追加数组

Json.parse 把字符串类型数组转成真正的数组

#### off()解绑事件

$('div').off('click') 解除div的点击事件

$('ul').off('click','li')解除委托事件

![image-20210420161307460](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420161307460.png)

#### one()触发一次事件

$('div').one('click') 触发一次div的点击事件

$('ul').one('click','li')触发一次委托事件

#### 自动触发事件

1  元素.事件()

2  元素.triggen('事件')

3          不会触发元素默认行为![image-20210420161857589](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420161857589.png)



####   event

#### 事件对象



#### 拷贝对象

#### jQ插件

![image-20210420163338441](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210420163338441.png)



推荐jQ之家  下载插件  引入插件 复制粘贴 



#### 多库共存

