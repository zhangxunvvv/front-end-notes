# js基础第一天大小写敏感

### javascript  > typescript

if else for while

script双标签

#### alert ('你好')

alert (’你好‘)   弹出动画框  额劳特

数据存储单位 

位:1bit可以保存一个0或者1  1b=8b

ECMAscript dom bom

外联式<script src="my.js"></script>

行内式  input type="button" value="唐伯虎" onclick=“alert”(秋香姐'')

齿轮>键盘快捷方式 设置快捷键

crtl+b 快速关闭资源管理器 

alt+上下方向键 快速上下移动代码

shift+alt+上下方向键 快速复制

### 输入输出

alert (msg) 弹出框

console.log(msg) 浏览器打印输出信息 程序员测试使用

prompt(info) 弹出输入框 用户输入 

### 变量 var

var age；声明一个名称为age的变量

age=10；给age赋值为10

= 是赋值的意思，不是相等 尽量不要用name44

var age =18；声明变量同时赋值

var age =18;

age=81;

结果为81，因为18被覆盖掉了

只声明不赋值会导致undefined 未定义的

一次声明多个变量用逗号分开

先声明后使用，var name prompt('请输入')

const常量不能被修改 否则报错 必须有初始值

下面有波浪线就是写法有误

temp声明一个变量为空

### 数据类型



js的变量数据类型是在程序运行当中根据等号右边的值判断的

var a = 10；  数字型或数值型 number  默认0

var a ='pink'   字符串型  string  

boolean  布尔值 代表真假 flase true默认flase 等价0和1

undefined 声明了但是没有给值

null  空值    属于object 数据类型

typeof 检测数值类型 log(typeof A)



#### log输出的文字是黑色代表字符型

蓝色是数值型



### 数字型特殊情况

八进制 0开头 逢7进1 010就是8 

十六进制 0x十六进制

number.max_value数值最大值 min_value最小值

*2就是无穷大    负的 * 2就是无穷小  nan非数字

console.log(isnan(pink老师))判断是否非数字false假 true真 

嵌套关系外双内单或者外单内双

#### 转义符

都是\开头，都写在引号里面

\n换行   \ \斜杠   \ '单引号   \ "双引号   \t缩进   \b空格    \r\n也是换行  

单引号不支持换行 反引号支持换行

log(a.length)获取整个字符串长度

数值相加 字符相连

log ('pink' + age + '岁') 引引加加

## 数值转换

##### 转换为字符串

变量·tostring()      string(变量)     和字符串相加

##### 转成数值型

log(parseInt(变量)函数)将字符串类型转化为整数数值型自动去掉后面的不是数字的文字，前面是文字会（nan）

parseFloat(string) 将string转化为浮点数数值型

Nunber() 强制转换函数 将string类型转换为数值型

js隐式转换(- * /) 利用算数运算隐式转换为数值型 

示例    log(parseInt(age));

#### boolean（）布尔值转换

没有值或者否定的是false  有值的就是true

&&且的意思   | |或的意思     !  非

&&两侧有一个fales就是假

### 三元表达式

条件表达式 ？ 表达式1真 ： 表达式2假

var num =10

var ccc=num>10 ? 是 : 否

alert(ccc)

age +=5相当于age=age+5

age-=5相当于age=age-5

age*=10相当于age * 10





### switch多分支语句，可以多选一

switch  开关的意思

switch(表达式) {

   case value1:

​            执行语句1;

​            break;

​    case value2:

​             执行语句2;

​             break;

​              ...

​             default:执行最后语句

}













# js-循环

## for循环 非常重要

for (var i = i; i <= 1000; i++) {

console.log('媳妇我错了')}

重复执行某些代码，跟(计数)次数有关系

#### for （ 初始化变量； 条件表达式；操作表达式 ）

 初始化 就是用var 声明的一个普通变量 通常作为计数器

条件表达式 决定每次循环是否继续或终止

操作表达式 每次循环最后执行的代码 经常用于计数器变量的更新（递增或递减） 

## while循环

while (条件表达式) {循环体}

var a=1

while (a<=100) {

a++、                                  

}

### 循环退出循环关键字

continue 立即退出本次循环，开始下一次循环 

break 退出整个循环

## 数组

数组是从0开始计算的，

var a = [0,1,2,3,4]   必须有逗号

筛选第三个       log(a[2])

如果全部访问一遍就用for循环，

注意i必须从0开始，小于最大值log(a[i])

#### 数组的数量，用 数组名.length

 查询数组数量（长度)   a.length

a.length = 5   增加数组数量

a[3] = ‘pink’    追加一个新的数组

新的覆盖老   老的会被替换

新字符串覆盖数组名会直接覆盖数组！

## 冒泡排序



![image-20210405174202815](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210405174202815.png)



## 函数

声明函数 function  函数名(形参1){函数体}

调用函数     函数名（实参1）

函数不调用不生效。

封装就是以函数的方式封装起来

形参，实参

#### 函数的返回值

function 函数名(){

return需要返回的结果

}

只要函数遇到return 就把后面的结果返回给调用者。

return 终止函数 只能返回一个值

函数没有teturn 返回的是undefined





### arguments的使用

	当不确定有多少个参数传递的时候，可以用 arguments 来获取。在 JavaScript 中，arguments实际上它是当前函数的一个内置对象。所有函数都内置了一个 arguments 对象，arguments 对象中存储了传递的所有实参。
	
	arguments展示形式是一个伪数组，因此可以进行遍历。伪数组具有以下特点：

- 具有 length 属性
- 按索引方式储存数据

### 对象

charAt()   1获取某个位置的字符串

var obj = {
    "name":"张三疯",
    "sex":"男",
    "age":128,
    "height":154
}

创建空对象

var andy = new Obect();

追加空对象属性和方法

andy.name = 'pink';
andy.age = 18;
andy.sex = '男';
andy.sayHi = function(){
    alert('大家好啊~');
}

#### 遍历对象    循环

for (var k in obj) {
    console.log(k);      // 这里的 k 是属性名
    console.log(obj[k]); // 这里的 obj[k] 是属性值
}

for (var k in obj) {
    console.log(k);      // 这里的 k 是属性名
    console.log(obj[k]); // 这里的 obj[k] 是属性值
}

 // 格式化日期 年月日 

​    var date = new Date();

​    console.log(date.getFullYear()); // 返回当前日期的年 2019

​    console.log(date.getMonth() + 1); // 月份 返回的月份小1个月  记得月份+1 呦

​    console.log(date.getDate()); // 返回的是 几号

​    console.log(date.getDay()); // 3 周一返回的是 1 周六返回的是 6 但是 周日返回的是 0

​    // 我们写一个 2019年 5月 1日 星期三

​    var year = date.getFullYear();

​    var month = date.getMonth() + 1;

​    var dates = date.getDate();

​    var arr = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'];

​    var day = date.getDay();

​    console.log('今天是：' + year + '年' + month + '月' + dates + '日 ' + arr[day]);

### 时间对象的使用

天var d = parseInt(times / 60 / 60 / 24);

​    时var h = parseInt((times / 60 / 60) % 24);

   分 var m = parseInt((times / 60) % 60);

   秒 var s = parseInt(times % 60);

#### 随机数 

function getRandom(min, max) {

​    return Math.floor(Math.random() * (max - min + 1)) + min;

   }

   var aa = getRandom(1, 50);

var now = new Date();

log(date.getDay())

<img src="file://C:\heima\就业班0312\就业班资料\JS基础6天资料\js-day06\5-笔记\images\图片4.png?lastModify=1617599279" alt="img" style="zoom: 200%;" />

#### 获得总毫秒数

// 实例化Date对象
var now = new Date();
// 1. 用于获取对象的原始值
console.log(date.valueOf())	
console.log(date.getTime())	
// 2. 简单写可以这么做
var now = + new Date();			
// 3. HTML5中提供的方法，有兼容性问题
var now = Date.now();

![image-20210405132331250](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210405132331250.png)

#### new Array() 创建一个空数组

var arr = new array

​	注意：上面代码中arr创建出的是一个空数组，如果需要使用构造函数Array创建非空数组，可以在创建数组时传入参数

​	参数传递规则如下：

- 如果只传入一个参数，则参数规定了数组的长度

- 如果传入了多个参数，则参数称为数组的元素

#### 检查是否为数组

instanceof 可以判断一个对象是否是某个构造函数的实例

比如array就是数组 

var arr = [1, 23];

var obj = {};

console.log(arr instanceof Array); // true

console.log(obj instanceof Array); // false

#### Array.isArray()判断是否是数组

Array.isArray()用于判断一个对象是否为数组，isArray() 是 HTML5 中提供的方法



### 添加删除数组元素的方法

<img src="file://C:\heima\就业班0312\就业班资料\JS基础6天资料\js-day06\5-笔记\images\图片2.png?lastModify=1617599938" alt="img" style="zoom:75%;" />

### 数组的删除api

var arr  = [1,2,3]

push(参数1...) 末尾添加一个或多个     例   log(arr.push(4,5,6))

pop ()   删除最后一个数组     返回删除地元素

unshift(6,7)  从前面开始加     

  arr.reveres() 数组反转

#### sort 排序

var a = [2, 6, 9, 5, 4, 1];

   a.sort(function (a, b) {

​    return a - b;

   });

   console.log(a);

#### 返回索引值

log(arr.indexOf('bkue'))  返回该数组元素的索引号,没有返回-1

lastIndexOf  ('')   从后往前

#### 数组转换为字符串

log(arr.toString())

jojn(分隔符)    例   jijn('-')

arr.splice(1,2)   从第二个开始删除两个

concat() 合并数组

#### 基本包装类型

把简单数据类型包装成复杂

#### 查找字符串里面字符



![image-20210405154648265](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210405154648265.png)

![image-20210405163332131](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210405163332131.png)

![image-20210405170720991](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210405170720991.png)

substr (1,2) 从2截取两个

replace('a','c') a替换为c

split(',')   分隔符

typeof 检测数据类型   null返回的是obj

### 浅拷贝