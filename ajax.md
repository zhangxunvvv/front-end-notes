​            

## url地址

leecode  面试题网站



统一资源定位符

url组成的三部分

1客户端与服务器的通信协议

2存有该资源的服务器名称

3资源在服务器上的具体位置

![image-20210502203245785](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502203245785.png)

 **请求-处理-响应**

#### xhr对象

XMLHttpRequest 请求服务器数据资源

![image-20210502204356002](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502204356002.png)



#### get请求 获取

获取服务器资源

#### post 请求  发送

向服务器提交资源

$.get() 拿

$.post() 提交

$.ajax()  拿,提交

#### 函数写法

中括号里面是可选的意思

$.get(url,[data],[callback])

![image-20210502205409155](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502205409155.png)



#### post()

添加数据,带参数添加

![image-20210502211627215](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502211627215.png)



![image-20210503113909912](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210503113909912.png)

#### $.ajax(

$.ajax({

 type:'post'

 url:'链接'

success: 回调函数

})

![image-20210503113937852](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210503113937852.png)



![image-20210503141952401](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210503141952401.png)



#### 接口文档

![image-20210503143351067](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210503143351067.png)



![image-20210503143446730](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210503143446730.png)



empty() 清空儿子所有数据

trim()去除空格

attr() 获取自定义

#### 聊天机器人

#### resetui()去到底部   

audio音频标签

只要为指定新的src并指定autoplay就能自动播放

attr('srx','http') 设置属性



## 表单

数据采集,提交给服务器

表单的组成部分

![image-20210504154656543](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210504154656543.png)

#### form标签属性

![image-20210504154749360](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210504154749360.png)

#### action提交url



![image-20210504155010226](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210504155010226.png)



#### target什么地方打开

何处打开action URL

![image-20210504155206539](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210504155206539.png)

#### method提交方式

post更安全

![image-20210504155539269](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210504155539269.png)



#### enctype发送之前编码



![image-20210504160140821](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210504160140821.png)

![image-20210504160222568](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210504160222568.png)



#### 表单提交的缺点



![image-20210504160431508](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210504160431508.png)

#### jq监听表单提交事件

submit事件

#### 阻止提交跳转

e.preventDefault()阻止表单的提交和跳转



![image-20210504160613947](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210504160613947.png)

#### serialuze()获取表单

form类名.serialuze()

键值对存储

必须为每个表单添加name属性

user-name = '张三'&password=密码的值

#### 获取表单所有数据

serialize()     this指向

只获取form表单中设置了name的字段+

![image-20210504161424627](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210504161424627.png)



#### reset()重置表单的值

表单[0].reset()重置表单的值



#### 模板引擎

##### art-template    模板引擎引入

docs   安装

帮助我们解决字符串拼接问题

自动生成一个html页面

![image-20210505143531357](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505143531357.png)



#### 输出

![image-20210505144418991](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505144418991.png)

![image-20210505144437502](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505144437502.png)

#### 原文输出

标签可能导致直接显示到页面

{{@ 数据}} 前面加一个@符号就可以

#### if输出

![image-20210505151630726](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505151630726.png)

![image-20210505151815752](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505151815752.png)

#### 循环输出

![image-20210505151943984](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505151943984.png)

![image-20210505152341042](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505152341042.png)

#### 过滤器



![image-20210505152735329](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505152735329.png)

![image-20210505152757110](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505152757110.png)



## regTime是dateFormat形参

![image-20210505153031938](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505153031938.png)



#### 正则与字符串操作

正则.exec(string名)



检索正则的匹配,符合返回字符串,不符合返回null

![image-20210505191143375](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505191143375.png)

![image-20210505191311864](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505191311864.png)

replace('要替换的','替换的')



![image-20210505195949568](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505195949568.png)

#### 真值替换

![image-20210506103449521](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506103449521.png)

#### 简易版模板引擎40

![image-20210506104357410](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506104357410.png)

## XML

#### 创建xhr对象

#### 调用open函数

#### 调用send函数发送

#### 监听请求状态事件

readystatechange请求发生改变事件

#### 判断是否成功

固定格式

readystrte===4 服务器解析状态 

   1new           4 为完成

status===200  200代表联网成功

responseText 返回的所有数据本体

 ```js
var xhr = new XMLHttpRequest();
xhr.open('GET', 'http://www.liulongbin.top:3006/api/getbooks');
xhr.send();
xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status == 200) {
        var str = xhr.responseText;
        str = JSON.parse(str);
        console.log(str);
    }
};
 ```





![image-20210506113108683](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506113108683.png)

![image-20210506112230710](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506112230710.png)

![image-20210506112042263](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506112042263.png)

![image-20210505202631646](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505202631646.png)

readystatechange 请求发生改变事件

#### readyState请求状态2

![image-20210505204923051](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505204923051.png)



#### XHR发起get请求

![image-20210505205114734](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505205114734.png)

#### 

#### 带参数请求,

在open链接后面?号添加参数,

![image-20210505205256877](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505205256877.png)

#### 查询字符串

?拼接的是查询字符串

![image-20210505205359364](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505205359364.png)

## url编码

![image-20210505210113945](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505210113945.png)

#### 编码与解码

encodeURI() 编码

decodeURI() 解码

```JS
 console.log(encodeURI('白马程序员'));
console.log(decodeURI('%E7%99%BD'));//白
```





![image-20210505210317429](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505210317429.png)

![image-20210506142842938](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506142842938.png)

#### post请求



```js
 var xhr = new XMLHttpRequest();
      xhr.open('POST', 'http://liulongbin.top:3006/api/addbook');
      //设置请求头
      //'application/x-www-form-urlenc'发送前编码
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlenc');
      xhr.send('bookname=水浒传&author=施耐庵&publisher=上海图书出版社');
      //请求发生改变事件			
      xhr.onreadystatechange = function () {
        //状态为4代表请求完成,200代表里联网成功
        if (xhr.readyState == 4 && xhr.status == 200) {
          console.log(xhr.resposeText);
        }
      };			
```



![image-20210506143859566](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506143859566.png)

#### axios

![image-20210508113234996](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508113234996.png)

![image-20210508113540792](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508113540792.png)

##### post请求

![image-20210508113612967](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508113612967.png)

![image-20210508114102442](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508114102442.png)



json数据格式 

## XML

标记语言

#### json

本质就是字符串

js 就是js里数组和字符串的表示方法

![image-20210505211008468](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505211008468.png)



![image-20210505211125494](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505211125494.png)

![image-20210505211305514](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210505211305514.png)

js对象转化json

![image-20210506151917562](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506151917562.png)

![image-20210506151956920](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506151956920.png)

join(&)转化为字符串以&分割

toUpperCase()字符串转化为大写

Ajax/

![image-20210506154819842](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506154819842.png)

#### 新特性                                                                                           

#### 设置http请求时限

![image-20210506162647447](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506162647447.png)

![image-20210506162904164](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506162904164.png)

#### 模拟表单操作

![image-20210506163247258](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506163247258.png)

![image-20210506163634344](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506163634344.png)

#### Form Data请求体

Form Data快速获取

只能用post方式,快速获取表单有name的值

autocomplete="off" 禁止记录输入以前的输入文字

#### 上传文件

files,数组,上传的文件,可以根据length判断是否上传的有文件



![image-20210508091643952](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508091643952.png)



![image-20210508092313876](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508092313876.png)

![image-20210508092447097](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508092447097.png)

![image-20210508092627793](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508092627793.png)



![image-20210506164511461](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506164511461.png)



![image-20210506165131594](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506165131594.png)

#### 上传进度条

设置在send之前

xhr.upload.onprogress上传进度事件

![image-20210508101830049](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508101830049.png)

#### 绘制进度条

bootstrap组件

![image-20210508101453731](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508101453731.png)

![image-20210508103029031](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508103029031.png)

#### jq实现文件上传



![image-20210508103554483](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508103554483.png)

![image-20210508103516150](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508103516150.png)

**请求开始事件**

![image-20210508104243637](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508104243637.png)

**请求完成结束完成事件**

![image-20210508104533964](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508104533964.png)

## 同源跨域

#### 协议,域名,端口完全 相同



![image-20210506180025155](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506180025155.png)

#### 同源策略

![image-20210506180302796](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506180302796.png)

#### 跨域



![image-20210506180439215](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506180439215.png)

![image-20210506180612053](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506180612053.png)

#### 实现跨域请求

#### JSONP  

**低版本专用,只支持get**

#### CORS  

**新版本,兼容性问题,get.post都支持**

![image-20210506180821678](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506180821678.png)



#### callback

#### jsonp

![image-20210506182935375](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506182935375.png)



![image-20210506183409312](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210506183409312.png)

jsonp:'Callback'    服务器属性名

jsonpCallback:      自定义函数名

#### 淘宝搜索



![image-20210508161904127](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508161904127.png)

![image-20210508185452507](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508185452507.png)



## 防抖



![image-20210508195410477](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210508195410477.png)



#### 缓存对象

定义一个缓存对象

![image-20210509092317116](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210509092317116.png)

#### 节流

![image-20210509094414873](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210509094414873.png)



![image-20210509094547888](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210509094547888.png)





140.82.114.4 github.com

199.232.69.194 github.global.ssl.fastly.net

185.199.108.153 assets-cdn.github.com

185.199.110.153 assets-cdn.github.com

185.199.111.153 assets-cdn.github.com





工作区-暂存区-Git仓库

![image-20210509102643021](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210509102643021.png)



#### 作者名字邮箱

git config --global user.name "zhangxun"
git config --global user.email "zhangxunvvv@163.com"

#### 查看所有的全局配置项
git config --list --global
#### 查看指定的全局配置项
git config user.name
git config user.email

### 获取帮助信息

可以使用 `git help <verb>` 命令，无需联网即可在浏览器中打开帮助手册，例如：

```shell
# 打开 git config 命令的帮助手册
git help config
```

如果不想查看完整的手册，那么可以用 -h 选项获得更简明的“help”输出：

#### 想要获取 git config 命令的快速参考
git config -h



#### git status检查状态

git status -s 精简形式检查状态



??就是未被追踪





![image-20210509111818285](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210509111818285.png)



![image-20210509141657483](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210509141657483.png)





![image-20210509151300445](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210509151300445.png)



![image-20210509152032314](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210509152032314.png)

#### glod模式

![image-20210509152123224](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210509152123224.png)



![image-20210509153656330](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210509153656330.png)





![image-20210509154157947](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210509154157947.png)



![image-20210509162434356](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210509162434356.png)



![image-20210511091453935](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210511091453935.png)



![image-20210511091725329](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210511091725329.png)

#### 生成ssh密钥

```
 ssh-keygen -t rsa -b 4096 -C "zhangxunvvv@163.com"
```



ssh -T git@gitee.com   链接gitee账号

#### 下载仓库代码

git clone "ssh链接"





![image-20210511102356789](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210511102356789.png)

**主分支-功能分支(生命短)-合并分支-主分支**

git branch  查看所有分支



![image-20210511112541286](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210511112541286.png)



切换到主分支

在主分支进行切换



![image-20210511114353314](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210511114353314.png)

#### 推送远程分支

如果是**第一次**将本地分支推送到远程仓库，需要运行如下的命令：

```shell
# -u 表示把本地分支和远程分支进行关联，只在第一次推送的时候需要带 -u 参数
git push -u 远程仓库的别名 本地分支名称:远程分支名称

# 实际案例
git push -u origin payment:pay

# 如果希望远程分支的名称和本地分支名称保持一致，可以对命令进行简化
git push -u origin payment
```

## 查看远程仓库中所有的分支列表

通过如下的命令，可以查看远程仓库中，所有的分支列表的信息：

```shell
git remote show 远程仓库名称
```

## 

## 跟踪分支(⭐⭐⭐)

跟踪分支指的是：从远程仓库中，把远程分支下载到本地仓库中。需要运行的命令如下：

```shell
# 示例
git checkout pay

# 从远程仓库中，把对应的远程分支下载到本地仓库，并把下载的本地分支进行重命名
git checkout -b 本地分支名称 远程仓库名称/远程分支名称

# 示例
git checkout -b payment origin/pay
```

## 拉取远程分支的最新的代码

可以使用如下的命令，把远程分支最新的代码下载到本地对应的分支中:

```shell
# 从远程仓库，拉取当前分支最新的代码，保持当前分支的代码和远程分支代码一致
git pull
```

## 删除远程分支

可以使用如下的命令，删除远程仓库中指定的分支：

```shell
# 删除远程仓库中，制定名称的远程分支
git push 远程仓库名称 --delete 远程分支名称

# 示例
git push origin --delete pay
```

# 总结

- 能够掌握 `Git` 中基本命令的使用
  - `git init`
  - `git add .`
  - `git commit –m "提交消息"` 
  - `git status` 和 `git status -s`
- 能够使用 `Github` 创建和维护远程仓库
  - 能够配置 `Github` 的 `SSH` 访问
  - 能够将本地仓库上传到 `Github`
- 能够掌握 `Git` 分支的基本使用
  - `git checkout -b 新分支名称`
  - `git push -u origin 新分支名称`
  - `git checkout 分支名称`
  - `git branch`

























