![image-20210424131722311](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210424131722311.png)

constructor(){}

![f](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210424133333785.png)



## super()调用父传给父类

super(x,y) 调用父类构造函数 数据传给父类

super.say()调用父类中的普通函数

​      继承:就近原则    子有用子 没有用父

​      **super()必须写在this.之前** 否则会报错

类没有变量提升,先定义类再实例化

所有共有属性和方法都要加this

外面的this赋值给一个全局变量,然后全局变量的name,this指向为btn这样就能调用外面的this指向的数值了

![image-20210503175422526](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210503175422526.png)

#### 高级创建///追加

insertAdjacentHTML()可以直接把字符串格式元素追加到父元素中



![image-20210425190437900](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210425190437900.png)

## 双击事件

ondblclick双击事件

禁止选中 window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();

.select() 保持选中状态

#### 函数原型

#### 原型对象

prototype对象,原型对象  方法共享 

一般情况下,我们的公共属性定义到构造函数里面,公共地方法放到原型对象上面

函数外面追加一个prototype里面的方法

![image-20210426195327650](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210426195327650.png)

![image-20210427144912845](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210427144912845.png)



#### 对象的原型

_ _ proto _ _    对象原型

指向原型对象  系统自动添加的



![image-20210426195857950](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210426195857950.png)



constructor  指回构造函数本身*****************

constructor: Star,重新指回函数Star

对象赋值给prototype会导致constructor被覆盖为没有,所以要重新指回本来\函数



![image-20210427154050823](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210427154050823.png)

![image-20210427161752714](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210427161752714.png)



#### 查找机制



![image-20210426203836507](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210426203836507.png)

原型链

![image-20210426204131248](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210426204131248.png)



添加一个求和

![image-20210426204838917](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210426204838917.png)



![image-20210426205301098](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210426205301098.png)



#### 组合继承

call() 调用函数并且修改函数运行时的this指向

函数.call(thisArg,参数1,参数二,...)

thisArg : 当前调用函数this的指向对象

fn.call() 调用函数

fn..call(o)改变this指向为o对象

fn.call(o,x,y) o为this指向,x y是普通参数



![image-20210429102825944](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429102825944.png)

![image-20210429104458509](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429104458509.png)



![image-20210429110100436](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429110100436.png)



#### 继承



父亲.call(this,uname,age)

把父构造函数this改为子构造函数

呼叫调用函数

this:改变函数内this的指向,

this默认指向window

![image-20210429094155411](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429094155411.png)



类的本质就是一个构造函数

特点

![image-20210429112150933](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429112150933.png)



![image-20210429112713168](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429112713168.png)

#### 数组遍历 迭代

增强for循环

![image-20210427200506895](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210427200506895.png)

##### 数组.forEach(fn(每个值,索引,数组本身))

return   value %2 == 0  返回能被2整除的新数组



值,索引,数值本身名字可以自定义,

索引,数组本身可以省略       

![image-20210427200536916](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210427200536916.png)

#### filter()

循环所有数组数据,把满足的返回到新的数组里面

#### 数组查找

#### some()

返回值是布尔值

找到满足条件的不会继续执行,直接中断返回值



![image-20210428162414288](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210428162414288.png)





#### 去除字符串两侧空格

字符串名.trim()

返回新字符串注意接收,不会去掉中间的

# vue主要原理

#### 获取对象身上的所有属性

Object.keys

![image-20210429152645286](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429152645286.png)



#### 修改对象属性

obj是对象 prop是对象属性名 descriptor有4个属性

value是属性值

枚举 - 遍历的意思

configurable: false不允许删除  不允许再次修改特性



![image-20210429153239996](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429153239996.png)

![image-20210429153447131](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429153447131.png)



## this指向

#### 改变this指向的方式

普通函数指向window

对象函数 指向调用者

构造函数new一下后 指向new的实例对象

​         原型对象里面的this也是指向实例对象

点击事件指向的是点击的元素,也就是调用者

定时器指向的是window,因为调用者是window

立即执行函数指向也是调用者window,



![image-20210429162526582](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429162526582.png)



## 改变this指向

call   调用函数,改变函数内this指向

apply()  调用函数,改变this指向 参数是数组形式

bind() 不会调用函数,可以改变this指向

#### call 方法,

调用函数,改变函数内this指向

   儿子可以实现**继承**         父亲.call(this,x,y)

函数名.coll(想指向的对象,x,y)



#### apply()

调用函数,改变this指向

后面参数必须是数组

函数.apply(函数名,[x,y])

**主要用来数组的操作 数学的计算**



Math.max.apply(null,数组名)

不建议null, 谁调用写谁就行 null改成Math





![image-20210429163425528](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429163425528.png)



#### bind()方法

不会调用函数,但是可以改变this指向

返回的是原函数改变this后产生的新函数,重新赋值

函数名.bind(指向的函数名,x,y)

用途想改变this指向不想立即调用函数.



![image-20210429164511464](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429164511464.png)

![image-20210429164710712](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429164710712.png)

#### ![image-20210430091414056](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210430091414056.png)



##### 箭头函数没有this,

![image-20210430094315939](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210430094315939.png)

## 严格模式

es5之后   IE10以上才会支持,

浏览器还有一种怪异模式

![image-20210429183850771](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429183850771.png)

**"use strict" 为脚本开启严格模式**

写在脚本最上面

 

![image-20210429190913653](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429190913653.png)



#### 高阶函数

参数有函数,





## 闭包closure

有权访问另一个函数作用域中变量的函数

外面被访问的函数就叫闭包

## 递归

函数内部自己调用自己就是递归函数

跟循环一样,反复调用 

必须加return防止栈溢出



![image-20210430145013786](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210430145013786.png)

#### 斐波那契数列

![image-20210430152239618](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210430152239618.png)



#### 递归查询更深一层的商品

![image-20210430153530849](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210430153530849.png)

#### 返回数据

![image-20210430154150899](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210430154150899.png)



![image-20210429201029717](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429201029717.png)

## 浅拷贝,深拷贝

#### 浅拷贝

for (var k in Obj)

##### 语法糖

es6新增方法 

Object.assiign(拷贝给谁,拷贝的对象)



#### 深拷贝

判断数据类型,  instanceof Array



![image-20210430162715212](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210430162715212.png)

![image-20210429210325047](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210429210325047.png)





# 正则表达式

正则表达式不加引号

#### 检测正则

test()检测是否符合规范

正则变量名.test(this.value)

regexObj.test(str)



创建正则表达式对象

var regxp = new RegExp(/123/)

字面量创建正则表达式

var 变量名 = /123/

#### 边界符

^  以什么开始

$ 以什么结尾

[^]  取反

a-z  范围取值



var a =/ ^abc$/

#### 字符类

var rg = /[abc]/ 随便包含任何一个就为true 

​         如  rg.test('andy')是true

 /^[abc]$/  三选一,aa为false

[a-z] 范围    识别大小写

/^[a-z1-9A-Z-_ ]$/

/^[ ^ a-z1-9A-Z-_ ]$/  中括号里面写^是取反

#### 量词

某个模式出现的次数

/^a?$/       1||0   1次或0次

/^a{1,}$/ 最少出现一次  

![image-20210502101440012](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502101440012.png)

() 小括号表示优先级,包括成一个整体

#### 在线测试工具

![image-20210502104837663](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502104837663.png)

#### 预定义类

小写正常  大写取反

​       |     或者的意思

 

![image-20210502111504882](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502111504882.png)

#### 案例

![image-20210502140836725](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502140836725.png)



replace('替换谁','替换成什么')下

g  全局   replace(/激情/g,'**')   全局匹配





## let关键字

具有块级作用域,没有变量提升

{}包裹的内容就叫作用域

**防止循环变量变成全局变量,**

#### 暂时性死区

let声明地变量会和块级作用域绑定

面试题

![image-20210502145049067](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502145049067.png)



#### const常量

值不能改



#### 总结

![image-20210502152232462](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502152232462.png)



解构

![image-20210502152553196](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502152553196.png)

#### 

对象结构将对象里面地属性名写在解构里面

![image-20210502152950017](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502152950017.png)



#### 箭头函数

当箭头函数方法体只有一行代码可以省略花括号和return0????

箭头函数没有自己this

let fn = (a,b) => a+b

当箭头函数形参只有一个,可以省略()括号

let fn = a => a+10

#### 剩余参数

...前面加三个点,代表接收所有实参

![image-20210502161624450](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502161624450.png)



![image-20210502162202967](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502162202967.png)



#### 扩展运算符



![image-20210502162652714](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502162652714.png)



#### 合并数组

![image-20210502163000124](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502163000124.png)

push()追加

![image-20210502163217286](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502163217286.png)



##### 伪数组转换成真正的数组

![image-20210502163702634](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502163702634.png)



#### 伪数组

array.from(伪数组名,函数) 



![image-20210502164054039](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210502164054039.png)



#### 数组名.find()

找到一个符合条件地对象

数组名.find(function(所有对象,index){

return 所有对象.id==2

})

#### 筛选数组

数组名.filder()

#### 包含

includes('a') 精确匹配,返回布尔

#### string头尾检测

返回布尔值

![image-20210503092823983](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210503092823983.png)

![image-20210503093045822](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210503093045822.png)



#### 重复字符串n次

'我爱你'.repeat(999)       重复字符串n次



#### set去重

去重

const s = new   Set()

log(s.size)

const s = new    set(['1','2','3',])

![image-20210503094525745](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210503094525745.png)



![image-20210503094726505](C:\Users\86155\AppData\Roaming\Typora\typora-user-images\image-20210503094726505.png)